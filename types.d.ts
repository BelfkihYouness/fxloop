declare module gloop {

    export let render: { (tween: number): void } | null;
    export let update: { (elapsed: number): boolean } | null;
    export let simulate: { (timestep: number): void } | null;

    export let timestep: number;
    export let ups: number;

    export function run(): void;
    export function stop(): void;

}

declare module "gloop" { export = gloop; }